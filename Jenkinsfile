/*
DESCRIPTION: This jenkins pipeline builds a given java
  repository on github and performs the following tasks
  before uploading to artifactory.
STAGES:
  CHECKOUT: gets the git repository
  BUILD: Builds the vRA/vRO packages
  TEST: Run tests on vRO Actions
  SCANS:
    OWASP: Dependency check
    SONAR: Security scan
*/

pipeline {
    agent any

    environment {
        // REPOSITORY_NAME = "${env.GIT_URL.tokenize('/')[3].split('\\.')[0]}"
        // REPOSITORY_OWNER = "${env.GIT_URL.tokenize('/')[2]}"
        // GIT_SHORT_REVISION = "${env.GIT_COMMIT[0..7]}"
        DISABLE_DOWNLOAD_PROGRESS_OPTS = '-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn '
        // ITESTS = 'distribution/test/itests/test-itests-alliance'
        // POMFIX = 'libs/pom-fix-run'
        // LARGE_MVN_OPTS = '-Xmx8192M -Xss128M -XX:+CMSClassUnloadingEnabled -XX:+UseConcMarkSweepGC '
        // LINUX_MVN_RANDOM = '-Djava.security.egd=file:/dev/./urandom'
        // COVERAGE_EXCLUSIONS = '**/test/**/*,**/itests/**/*,**/*Test*,**/sdk/**/*,**/*.js,**/node_modules/**/*,**/jaxb/**/*,**/wsdl/**/*,**/nces/sws/**/*,**/*.adoc,**/*.txt,**/*.xml'
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '25'))
        disableConcurrentBuilds()
        timestamps()
        // skipDefaultCheckout()
        disableResume()
    }
    triggers {
        //  Nightly builds for master branch
        cron(BRANCH_NAME == "master" ? "H H(19-21) * * *" : "")
    }

    tools {
        maven "icds-maven"
    }

    stages {
        stage('Checkout') {
            steps {
                //  slackSend color: 'good', message: "STARTED: ${JOB_NAME} ${BUILD_NUMBER} ${BUILD_URL}"
                //  postCommentIfPR("Internal build has been started, your results will be available at build completion.", "${GITHUB_USERNAME}", "${GITHUB_REPONAME}", "${GITHUB_TOKEN}")

                // sh '''
                //     echo "PATH = ${PATH}"
                //     echo "M2_HOME = ${M2_HOME}"
                //     echo "REPOSITORY_NAME = ${REPOSITORY_NAME}"
                //     echo "REPOSITORY_OWNER = ${REPOSITORY_OWNER}"
                //     echo "GIT_SHORT_REVISION = ${GIT_SHORT_REVISION}"
                // '''
                retry(3){
                    checkout scm
                }
            }
        }
        stage('Build') {
            options {
                timeout(time: 1, unit: 'HOURS')
            }
            steps {
                withMaven(maven: 'icds-maven') {
                    sh 'mvn -U -B -DskipTests clean package deploy'
                }
            }
        }
        stage("Quality") {
            parallel {
                stage('Test') {
                    steps {
                        withMaven(maven: 'icds-maven') {
                            sh 'mvn -U -B test'
                        }
                    }
                }
                stage('SonarQube') {
                    steps {
                        withMaven(maven: 'icds-maven') {
                            sh 'echo SonarQube'
                            sh 'mvn -U -B sonar:sonar -DskipTests'
                        }
                    }
                }
                stage('Dependency Check') {
                    steps {
                        withMaven(maven: 'icds-maven') {
                            sh 'echo Dependency Check'
                            sh 'mvn -U -B org.owasp:dependency-check-maven:check -DskipTests'
                        }
                    }
                }
            }
        }        
        // stage('Dependency Check') {
        //     steps {
        //         withMaven(maven: 'M35', jdk: 'jdk8-latest', globalMavenSettingsConfig: 'default-global-settings', mavenSettingsConfig: 'codice-maven-settings', mavenOpts: '${LARGE_MVN_OPTS} ${LINUX_MVN_RANDOM}') {
        //             script {
        //                 // If this build is not a pull request, run full owasp scan. Otherwise run incremental scan
        //                 if (env.CHANGE_ID == null) {
        //                     sh 'mvn org.commonjava.maven.plugins:directory-maven-plugin:highest-basedir@directories dependency-check:check dependency-check:aggregate -q -B -Powasp -DskipTests=true -DskipStatic=true -pl !$DOCS $DISABLE_DOWNLOAD_PROGRESS_OPTS'
        //                 } else {
        //                     sh 'mvn org.commonjava.maven.plugins:directory-maven-plugin:highest-basedir@directories dependency-check:check -q -B -Powasp -DskipTests=true -DskipStatic=true -pl !$DOCS -Dgib.enabled=true -Dgib.referenceBranch=/refs/remotes/origin/$CHANGE_TARGET $DISABLE_DOWNLOAD_PROGRESS_OPTS'
        //                 }
        //             }
        //         }
        //     }
        // }

        // stage ('SonarCloud') {
        //     environment {
        //         SONARQUBE_GITHUB_TOKEN = credentials('SonarQubeGithubToken')
        //         SONAR_TOKEN = credentials('sonarqube-token')
        //     }
        //     steps {
//                     sh 'mvn -q -B -Dcheckstyle.skip=true org.jacoco:jacoco-maven-plugin:prepare-agent install sonar:sonar -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=$SONAR_TOKEN  -Dsonar.organization=codice -Dsonar.projectKey=org.codice:alliance -Dsonar.exclusions=${COVERAGE_EXCLUSIONS} -pl !$DOCS,!$ITESTS $DISABLE_DOWNLOAD_PROGRESS_OPTS'
        //         }
        //     }
        // }

        stage('Publish Non-Master') {
            when {
                not {
                    branch 'master'
                }
            }
            steps {
                sh 'echo Publish Non-Master'
                withMaven(maven: 'icds-maven') {
                    sh 'mvn deploy -U -B -DskipTests=true -DskipStatic=true -DretryFailedDeploymentCount=10 $DISABLE_DOWNLOAD_PROGRESS_OPTS'
                }
            }
        }
        stage('Publish Master') {
            when {
                branch 'master'
            }
            steps {
                sh 'echo Publish Master'
                withMaven(maven: 'icds-maven') {
                    sh 'mvn deploy -U -B -DskipTests=true -DskipStatic=true -DretryFailedDeploymentCount=10 $DISABLE_DOWNLOAD_PROGRESS_OPTS'
                }
            }
        }
        stage('Deploy to Dev') {
            when {
                not {
                    branch 'master'
                }
            }
            steps {
                sh 'echo Deploying to the dev environment'
                withMaven(maven: 'icds-maven') {
                    sh 'mvn package vrealize:push -U -Pvra74-mss-dev -B -DskipTests=true -DskipStatic=true $DISABLE_DOWNLOAD_PROGRESS_OPTS'
                }
            }
        }
        stage('Deploy to UAT') {
            when {
                branch 'master'
            }
            steps {
                sh 'echo Deploying to the UAT environment'
                withMaven(maven: 'icds-maven') {
                    sh 'mvn package vrealize:push -U -Pvra74-mss-dev -B -DskipTests=true -DskipStatic=true $DISABLE_DOWNLOAD_PROGRESS_OPTS'
                }
            }
        }

    // post {
    //     always {
    //          step([$class: 'Mailer',
    //              notifyEveryUnstableBuild: true,
    //              recipients: "nobody@us.ibm.com",
    //              sendToIndividuals: true
    //          ])
    //          postCommentIfPR("Build ${currentBuild.currentResult} See the job results in [legacy Jenkins UI](${BUILD_URL}) or in [Blue Ocean UI](${BUILD_URL}display/redirect).", "${GITHUB_USERNAME}", "${GITHUB_REPONAME}", "${GITHUB_TOKEN}")
    //     }
    //     success {
    //         slackSend color: 'good', message: "SUCCESS: ${JOB_NAME} ${BUILD_NUMBER}"
    //     }
    //     failure {
    //         slackSend color: '#ea0017', message: "FAILURE: ${JOB_NAME} ${BUILD_NUMBER}. See the results here: ${BUILD_URL}"
    //     }
    //     unstable {
    //         slackSend color: '#ffb600', message: "UNSTABLE: ${JOB_NAME} ${BUILD_NUMBER}. See the results here: ${BUILD_URL}"
    //     }
    //     aborted {
    //         slackSend color: '#909090', message: "ABORTED: ${JOB_NAME} ${BUILD_NUMBER}. See the results here: ${BUILD_URL}"
    //     }
    //     cleanup {
    //         echo '...Cleaning up workspace'
    //     }
    // }
    }
}
