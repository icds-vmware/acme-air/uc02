describe("sample", function() {
    var sample = System.getModule("com.acme.air.uc02").sample;
    it("should add two numbers", function() {
        expect(sample(5, 2)).toBe(7);
    });
});